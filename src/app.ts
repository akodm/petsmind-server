import dotenv from 'dotenv';
dotenv.config();
import express, { NextFunction, Request, Response } from 'express';
import path from 'path';
import logger from 'morgan';
import cors from 'cors';
import helmet from 'helmet';
import createError from 'http-errors';
import cookieParser from 'cookie-parser';
import indexRouter from './routes';
import userRouter from './routes/user';
import moment from 'moment';
import './sequelize';
moment.locale("ko"); // localization.

// port setup.
const normalizePort = (val: string) => {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
}

const { PORT, CLIENT_URL, NODE_ENV } = process.env;

const port = normalizePort(PORT ?? "3333");

const app = express();

const corsOptions = {
  origin: CLIENT_URL as string,
  optionsSuccessStatus: 200
};

// app options.
app.use(cors(corsOptions));
app.use(helmet());
app.use(logger("dev")); // + winston ?
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));  // static folder.

// app run.
app.listen(port, () => {
  console.log(`NODE_ENV: ${NODE_ENV}, PORT: ${PORT} Hello Typescript-Express ! ${moment().format("YYYY. MM. DD. (ddd) HH:mm:ss")}`);
  process.send && process.send("ready");
});

// + app.io = socket.io

// routes.
app.use("/", indexRouter);
app.use("/user", userRouter);

app.use((req: Request, res: Response, next: NextFunction) => {
  next(createError(404));
});

interface RequestError {
  message: string;
  status: number | 500;
  s?: number;
  m?: string;
}

app.use((err: RequestError, req: Request, res: Response) => {
  const status = err.s || err.status || 500;
  const message = err.m || "서버에서 문제가 발생하였습니다. 잠시 후 시도해주세요.";

  console.log(err);
  
  return res.send({
    result: false,
    data: null,
    message,
    status
  });
});

export default app;